//
//  AppDelegate.h
//  Frans Tutorial 2
//
//  Created by Vensi, Inc. on 10/31/13.
//  Copyright (c) 2013 Vensi, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) IBOutlet UIWindow *window;

@end
